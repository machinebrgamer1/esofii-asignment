FROM alpine:latest

RUN apk add --no-cache openjdk11 maven

RUN mkdir /opt/app

COPY . /opt/app

WORKDIR /opt/app

EXPOSE 8080

CMD ["mvn", "spring-boot:run"]